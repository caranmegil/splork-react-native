/*

App.js
Copyright (C) 2021  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import React, { useEffect, useState } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Button,
  View,
  Text,
  StatusBar,
  Share,
  Platform,
  Clipboard,
} from 'react-native';

const App = () => {
  const [splork, setSplork] = useState('');

  async function onPressSplork() {
    let response = await fetch('https://splork.nerderium.com/message');
    let json = await response.json()
    setSplork(json.message);
  }

  const onPressShare = async () => {
    const result = await Share.share({
      message: splork
    });
  }

  const onPressCopy = () => {
    Clipboard.setString(splork);
  }

  useEffect( () => {
    if (splork === '') {
      onPressSplork();
    }
  }, [splork] )
  
  return (
    <View>
      <StatusBar backgroundColor="#007F5C">
        <Text color="white">Splork1</Text>
      </StatusBar>
      <SafeAreaView style={styles.body}>
        <View>
          <View style={styles.toolbar}>
            <Text style={styles.toolbarText}>Splork</Text>
          </View>
          <View style={styles.sectionContainer}>
          { (Platform.OS === 'android' || Platform.OS === 'ios') ?
              <Button
                color="#007F5C"
                onPress={onPressShare} title="Share"></Button>
            : (Platform.OS === 'macos' || Platform.OS === 'windows') ?
              <Button
                color="#007F5C"
                onPress={onPressCopy} title="Copy"></Button>
            : null
          }
          </View>
          <View style={styles.sectionContainer}>
            <Button
              onPress={onPressSplork}
              title="Generate"
              color="#007F5C"
            />
          </View>
          <View style={styles.splorkContainer}>
            <Text style={styles.splorkText}>{splork}</Text>
          </View>
        </View>
      </SafeAreaView>
    </View>
  );
};

const styles = StyleSheet.create({
  engine: {
    position: 'absolute',
    right: 0,
  },
  toolbar: {
    backgroundColor: '#007F5C',
  },
  toolbarText: {
    fontSize: 24,
    color: 'white',
    padding: 5,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionContainer2: {
    position: 'relative',
    paddingHorizontal: 24,
  },
  buttonBottom: {
    position: 'absolute',
    bottom: 0,
    paddingHorizontal: 24,
  },
  splorkContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
    height: 500,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  splorkText: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
